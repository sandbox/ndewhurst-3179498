<?php

/**
 * @file
 * Administration form for Wufoo.
 */

/**
 * Builds the Settings form.
 */
function wufoo_settings_form() {
  $form = array();

  $form['wufoo_api_key'] = [
    '#type'          => 'textfield',
    '#title'         => t('Wufoo API Key'),
    '#description'   => t('The API key to use for all Drupal-Wufoo API interactions.'),
    '#maxlength'     => 32,
    '#default_value' => variable_get('wufoo_api_key'),
  ];

  $form['wufoo_subdomain'] = [
    '#type'          => 'textfield',
    '#title'         => t('Wufoo Subdomain'),
    '#description'   => t('The subdomain of the Wufoo account with which you want to integrate. E.g. if you your Wufoo URLs look like "mycompany.wufoo.com/etc," your subdomain is "mycompany."'),
    '#maxlength'     => 64,
    '#default_value' => variable_get('wufoo_subdomain'),
  ];

  $form = system_settings_form($form);

  return $form;
}
