<?php

/**
 * @file
 * Drupal API documentation.
 *
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * @defgroup wufoo_hooks Wufoo hooks
 * Hooks that can be implemented by other modules to extend Wufoo.
 *
 * @todo Add hook documentation.
 */
