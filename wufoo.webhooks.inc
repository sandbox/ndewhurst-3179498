<?php

/**
 * @file
 * Webhooks administration form for Wufoo.
 */

use Adamlc\Wufoo\WufooApiWrapper;

/**
 * Builds the Settings form.
 */
function wufoo_webhooks_admin_form() {
  $form = [];

  $wufoo_forms = [];
  $wufoo_form_options = [
    '_none' => t('Please choose a form'),
  ];
  $api_key = variable_get('wufoo_api_key', FALSE);
  $subdomain = variable_get('wufoo_subdomain', FALSE);
  if ($api_key && $subdomain) {
    try {
      $wufoo = new WufooApiWrapper($api_key, $subdomain);
      $wufoo_forms = $wufoo->getForms();
    } catch (Exception $e) {
      $wufoo_forms = FALSE;
    }

    if (!empty($wufoo_forms)) {
      $form_options = array_map(function ($wufooForm) {
        return $wufooForm->Name;
      }, $wufoo_forms);
      $wufoo_form_options = array_merge($wufoo_form_options, $form_options);
    }

    $query = db_select('wufoo_webhooks', 'wh')
      ->fields('wh')
      ->orderBy('wh.update_timestamp', 'DESC')
      ->execute();

    $existing_webhooks = [];
    if ($query->rowCount() > 0) {
      foreach ($query as $row) {
        $existing_webhooks[] = $row->webhook_hash;
        $webhook_hash = 'webhook_' . $row->webhook_hash;
        $form[$webhook_hash] = [
          '#type'  => 'fieldset',
          '#title' => $row->webhook_name,
        ];
        $form[$webhook_hash]["{$webhook_hash}_delete"] = [
          '#type'        => 'checkbox',
          '#title'       => t('Delete this Webhook'),
          '#default_value'       => 0,
          '#description' => t('Completely remove this webhook from Wufoo and from Drupal. It will no longer function.'),
        ];
        $form_hash = $row->form_hash;
        if (!isset($wufoo_form_options[$row->form_hash])) {
          $form_hash = '_none';
          $args = [
            '!form_name' => $row->form_name,
            '!form_hash' => $row->form_hash,
          ];
          $form[$webhook_hash]["{$webhook_hash}_form_notice"] = [
            '#value' => t('The form previously linked to this webhook is no longer available. Form name: !form_name. Form hash/ID: !form_hash.', $args),
          ];
        }
        $form[$webhook_hash]["{$webhook_hash}_form"] = [
          '#type'          => 'select',
          '#title'         => 'Wufoo Form',
          '#options'       => $wufoo_form_options,
          '#default_value' => $form_hash,
        ];
        $form[$webhook_hash]["{$webhook_hash}_name"] = [
          '#type'        => 'textfield',
          '#size'        => 64,
          '#title'       => t('Name'),
          '#value'       => $row->webhook_name,
          '#description' => t('A unique name for the webhook. This will be used to generate the webhook URL. Do not use spaces or special characters.'),
        ];
        $form[$webhook_hash]["{$webhook_hash}_url"] = [
          '#type'        => 'textfield',
          '#disabled'    => TRUE,
          '#title'       => t('URL'),
          '#value'       => $row->webhook_url,
          '#description' => t('The full URL of the webhook. Wufoo will send data to this URL. Generated automatically from the webhook name.'),
        ];
        $form[$webhook_hash]["{$webhook_hash}_handshake_key"] = [
          '#type'        => 'textfield',
          '#disabled'    => TRUE,
          '#title'       => t('Handshake Key'),
          '#value'       => $row->handshake_key,
          '#description' => t('A key that Wufoo will include with each webhook request. This provides additional security. Drupal automatically generates this key for you and ensures that it is provided with each request.'),
        ];
        $form[$webhook_hash]["{$webhook_hash}_include_metadata"] = [
          '#type'        => 'checkbox',
          '#title'       => t('Include Metadata'),
          '#value'       => $row->include_metadata,
          '#description' => t('Whether Wufoo should include form metadata with each request.'),
        ];
        $form[$webhook_hash]["{$webhook_hash}_hash"] = [
          '#type'          => 'value',
          '#title'         => t('Hash'),
          '#value'         => $row->webhook_hash,
          '#default_value' => '_none',
        ];
      }
    }

    $form['existing_webhook_hashes'] = [
      '#type' => 'value',
      '#value' => $existing_webhooks,
    ];
    $form['all_wufoo_forms'] = [
      '#type' => 'value',
      '#value' => $wufoo_form_options,
    ];

    // Allow users to create a new webhook using the same form.
    $form['new_webhook'] = [
      '#type'  => 'fieldset',
      '#title' => t('Create a new webhook'),
    ];
    $form['new_webhook']['new_webhook_form'] = [
      '#type'    => 'select',
      '#title'   => 'Wufoo Form',
      '#options' => $wufoo_form_options,
    ];
    $form['new_webhook']['new_webhook_name'] = [
      '#type'        => 'textfield',
      '#size'        => 64,
      '#title'       => t('Name'),
      '#description' => t('A unique name for the webhook. This will be used to generate the webhook URL. Do not use spaces or special characters.'),
    ];
    $form['new_webhook']['new_webhook_description'] = [
      '#type'        => 'textfield',
      '#size'        => 128,
      '#title'       => t('Description'),
      '#description' => t('Optional'),
    ];
    $form['new_webhook']['new_webhook_include_metadata'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Include Metadata'),
      '#description'   => t('Whether Wufoo should include form metadata with each request.'),
      '#default_value' => 0,
    ];

    $form['settings'] = [
      '#type'  => 'fieldset',
      '#title' => t('Webhook Settings'),
    ];

    $form['settings']['wufoo_webhooks_debug'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Log all webhook events (e.g. for debugging purposes)'),
      '#default_value' => variable_get('wufoo_webhooks_debug', FALSE),
    ];

    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => t('Submit'),
    ];
  }
  else {
    $form['missing_api_data'] = [
      '#markup' => '<p>' . t('Missing required API data. Please visit the Settings tab and enter your API key and subdomain.') . '</p>'
    ];
  }

  return $form;
}

/**
 * Form validation handler.
 */
function wufoo_webhooks_admin_form_validate($form, &$form_state) {
  $required_fields = [
    'new_webhook_name',
    'new_webhook_form',
  ];
  $new_webhook = FALSE;
  $missing_required_fields = [];
  foreach ($required_fields as $required_field) {
    if (!empty($form_state['values'][$required_field] && $form_state['values'][$required_field] != '_none')) {
      $new_webhook = TRUE;
    }
    else {
      $missing_required_fields[] = $required_field;
    }
  }
  if ($new_webhook) {
    foreach ($missing_required_fields as $missing_required_field) {
      $field_name = $form['new_webhook'][$missing_required_field]['#title'];
      form_set_error($missing_required_field, t('The @field_name field is required when creating a new webhook.', ['@field_name' => $field_name]));
    }
  }

  // Validate webhook name. Convert to desired format.
  if (!empty($form_state['values']['new_webhook_name'])) {
    $webhook_name = trim(strtolower($form_state['values']['new_webhook_name']));
    $webhook_name = preg_replace('/[^-a-z0-9]+/', '-', $webhook_name);
    $webhook_name = trim($webhook_name, '-');
    if (strlen($webhook_name) > 0) {
      $form_state['values']['new_webhook_name'] = $webhook_name;
    }
    else {
      form_set_error('new_webhook_name', t('The webhook name should only include letters, numbers, and dashes.'));
    }
  }
}

/**
 * Form submit handler.
 */
function wufoo_webhooks_admin_form_submit($form, &$form_state) {
  $api_key = variable_get('wufoo_api_key', FALSE);
  $subdomain = variable_get('wufoo_subdomain', FALSE);
  if ($api_key && $subdomain) {
    try {
      $wufoo = new WufooApiWrapper($api_key, $subdomain);

      $all_wufoo_forms = $form_state['values']['all_wufoo_forms'];

      // Update existing webhooks with the provided data, and delete any that
      // have been flagged for deletion.
      foreach ($form_state['values']['existing_webhook_hashes'] as $existing_webhook_hash) {
        $form_hash = $form_state['values']["webhook_{$existing_webhook_hash}_form"];
        $form_name = $all_wufoo_forms[$form_hash];
        $webhook_name = $form_state['values']["webhook_{$existing_webhook_hash}_name"];
        $webhook_url = $form_state['values']["webhook_{$existing_webhook_hash}_url"];
        $handshake_key = $form_state['values']["webhook_{$existing_webhook_hash}_handshake_key"];
        $include_metadata = $form_state['values']["webhook_{$existing_webhook_hash}_include_metadata"];

        if ($form_state['values']["webhook_{$existing_webhook_hash}_delete"]) {
          $deleted_webhook = $wufoo->webHookDelete($form_hash, $existing_webhook_hash);
          if (get_class($deleted_webhook) == 'Adamlc\Wufoo\Response\WebHookResponse') {
            drupal_set_message(t('Successfully deleted the !webhook_name webhook.', ['!webhook_name' => $webhook_name]));
          }
          else {
            drupal_set_message(t('A problem occurred while deleting the !webhook_name webhook. Please check Wufoo.', ['!webhook_name' => $webhook_name]), 'error');
          }
          db_delete('wufoo_webhooks')
            ->condition('webhook_hash', $existing_webhook_hash)
            ->execute();
        }
        else {
          // @todo: Only update existing webhook if relevant changes occurred.
          // @todo: Confirm whether hash remains the same when form is changed.
          $updated_webhook = $wufoo->webHookPut($form_hash, $webhook_url, $handshake_key, $include_metadata);
          if (get_class($updated_webhook) == 'Adamlc\Wufoo\Response\WebHookResponse') {
            drupal_set_message(t('Successfully updated the !webhook_name webhook.', ['!webhook_name' => $webhook_name]));
          }
          else {
            drupal_set_message(t('A problem occurred while updating the !webhook_name webhook. Please check Wufoo.', ['!webhook_name' => $webhook_name]), 'error');
          }
          $record = [
            'form_hash'           => $form_hash,
            'form_name'           => $form_name,
            'webhook_name'        => $webhook_name,
            'webhook_url'         => $webhook_url,
            'webhook_description' => (string) $form_state['values']['new_webhook_description'],
            'handshake_key'       => $handshake_key,
            'include_metadata'    => $include_metadata,
            'update_timestamp'    => REQUEST_TIME,
          ];
          db_update('wufoo_webhooks')
            ->condition('webhook_hash', $existing_webhook_hash)
            ->fields($record)
            ->execute();
        }
      }

      // Create a new webhook if applicable.
      if (!empty($form_state['values']['new_webhook_form'])) {
        // Create the webhook in Wufoo.
        $form_hash = $form_state['values']['new_webhook_form'];
        $form_name = $all_wufoo_forms[$form_hash];
        $webhook_name = $form_state['values']['new_webhook_name'];
        $webhook_url = url("wufoo/webhooks/$webhook_name", ['absolute' => TRUE]);
        $handshake_key = md5($webhook_name . (string) REQUEST_TIME);
        $include_metadata = $form_state['values']['new_webhook_include_metadata'];

        $created_webhook = $wufoo->webHookPut($form_hash, $webhook_url, $handshake_key, $include_metadata);
        if (get_class($created_webhook) == 'Adamlc\Wufoo\Response\WebHookResponse') {
          // Get the hash of the newly created webhook.
          $new_webhook_hash = $created_webhook->Hash;

          // Create a record in our DB table.
          $record = [
            'webhook_hash'        => $new_webhook_hash,
            'form_hash'           => $form_hash,
            'form_name'           => $form_name,
            'webhook_name'        => $webhook_name,
            'webhook_url'         => $webhook_url,
            'webhook_description' => (string) $form_state['values']['new_webhook_description'],
            'handshake_key'       => $handshake_key,
            'include_metadata'    => $include_metadata,
            'creation_timestamp'  => REQUEST_TIME,
            'update_timestamp'    => REQUEST_TIME,
          ];

          db_insert('wufoo_webhooks')
            ->fields($record)
            ->execute();

          drupal_set_message(t('Successfully created the !webhook_name webhook.', ['!webhook_name' => $webhook_name]));
        }
        else {
          drupal_set_message(t('A problem occurred while updating the !webhook_name webhook. Please check Wufoo.', ['!webhook_name' => $webhook_name]), 'error');
        }
      }
    }
    catch (Exception $e) {
      watchdog('wufoo', 'An error occurred while processing Wufoo webhook settings: !msg', ['!msg' => $e->getMessage()], WATCHDOG_ERROR);
    }
  }
  variable_set('wufoo_webhooks_debug', $form_state['values']['wufoo_webhooks_debug']);
}

/**
 * Helper function to get payload data from an incoming webhook POST.
 *
 * @return array|false|mixed|string
 */
function wufoo_webhook_get_data() {
  $payload = &drupal_static(__FUNCTION__);
  if (isset($payload)) {

    return $payload;
  }

  // We do not perform specific validation of POST params because we have
  // already validated the handshake key, which is required for all Wufoo-Drupal
  // webhooks.
  $payload = $_POST;

  return $payload;
}


/**
 * Page callback to respond to incoming requests from Wufoo webhooks.
 */
function wufoo_webhook_listener($webhook) {
  $webhook_name = $webhook->webhook_name;
  $webhook_slug = str_replace('-', '_', $webhook_name);
  if ($payload = wufoo_webhook_get_data()) {
    if (isset($payload['FormId'])) {
      if (variable_get('wufoo_webhooks_debug', FALSE)) {
        $form_id = $payload['FormId'];
        watchdog('wufoo_webhooks', 'A Wufoo webhook event occurred for form !form_id. Webhook name: !name. Payload: !payload', ['!form_id' => $form_id, '!name' => $webhook_name, '!payload' => serialize($payload)], WATCHDOG_DEBUG);
      }

      module_invoke_all("wufoo_webhook_$webhook_slug", $payload);
      module_invoke_all("wufoo_webhook", $webhook_slug, $payload);
    }
    else {
      watchdog('wufoo_webhooks', 'A Wufoo webhook event occurred with no FormId. Webhook name: !name. Payload: !payload', ['!name' => $webhook_name, '!payload' => serialize($payload)], WATCHDOG_ERROR);
    }
  }
  else {
    watchdog('wufoo_webhooks', 'A Wufoo webhook event occurred with no payload. Webhook name: !name.', ['!name' => $webhook->webhook_name], WATCHDOG_ERROR);
  }
}
