Wufoo Drupal Integration
-------------------------------------------------------------------------------
This module allows you to integrate with the Wufoo
(https://www.wufoo.com) form management platform via the Wufoo REST API, and
also enables convenient webhook management.

Dependencies
-------------------------------------------------------------------------------
 - A Wufoo account (see https://www.wufoo.com)
 - The Wufoo PHP SDK/library (see below).

Installation
-------------------------------------------------------------------------------
Install the module as you would install any other Drupal module. Use the
[Composer Manager](https://www.drupal.org/project/composer_manager) module to
ensure that the
[Wufoo PHP library](https://github.com/wufoo/Wufoo-PHP-API-Wrapper) is
correctly installed as well.

Initial Setup
-------------------------------------------------------------------------------
1. Log in to your Wufoo account
2. Locate a form with which you would like to integrate
3. Click the 3-button context menu next to the form, then select "API
Information"
4. Copy the API key
5. Visit /admin/config/services/wufoo/settings on your Drupal site
6. Enter the API key and save the settings form

You can also set the key in your site's settings.php file
(e.g. sites/default/settings.php) if desired.
$conf['wufoo_api_key'] = 'YOUR API KEY';

Configuration
-------------------------------------------------------------------------------

## Webhooks
This module makes it easy to manage webhooks so that Wufoo can notify your
Drupal site whenever a form is submitted.
To view/add/update webhooks, visit /admin/config/services/wufoo/webhooks.
